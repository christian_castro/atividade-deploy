import './App.css';
import Card from './components/Card/card';
import Developer from './components/Usuarios/developer';

const App = () => {

  const usuarios = 
    {
     nameOne: "Gabriel", ageOne: 19, countryOne: "Brasil", 
     nameTwo: "Filipe", ageTwo: 28, countryTwo: "Brasil",
     nameThree: "Mariana", ageThree: 24, countryThree: "Italia",
    }
    
  return (
    <div className="App">
        <Developer name={usuarios.nameOne} age={usuarios.ageOne} country={usuarios.countryOne}/>
        <Developer name={usuarios.nameTwo} age={usuarios.ageTwo} country={usuarios.countryTwo}/>
        <Developer name={usuarios.nameThree} age={usuarios.ageThree} country={usuarios.countryThree}/>
        <Card />
    </div>
  );
}

export default App;
