import './style.css'

function Developer({name, age, country}) {
    return (
        <div>
            <div className="dev">
                <h2>Dev: {name}</h2>
                <h3>Idade: {age}</h3>
                <h3>Country: {country}</h3>
            </div>
        </div>
    )
}

export default Developer